## osint river
![codebase](https://img.shields.io/badge/codebase-drawio-red)

<div align="center">
  <img src="images/fail.gif">
</div>

My osint flow...

### Getting started

TLDR; This repo contains my latest osint process flow. Each step is cyclic and can feed into any other step. Furthermore, I use it as a tick of box type process so I dont miss anything. Tool references are not included as its usually up to the operators choice.

### Reference

You can either download it and use drawio within vscode or just download/copy the drawio file/image and use it as you please!

```
git clone https://gitlab.com/interdiction_/osint-river
#whodareswins
```

#### Images

##### Flowchart

<div align="center">

<img src="images/river.png" width="1000">

</div>

##### Vscode editing

<div align="center">

<img src="images/vscode.png" width="900">

</div>